const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const verifyTask = require("./taskVerify")

module.exports = {
  authJwt,
  verifySignUp,
  verifyTask
};