const db = require("../models/index");
const ROLES = db.ROLES;
const User = db.user;

// Check if username or email is already taken / registered
checkUsernameOrEmailDupl = (req, res, next) => {
    // Check Username
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if(user) {
            res.status(400).send({
                message: "Oops! Username already taken!"
            });
            return;
        }

        // Check Email
        User.findOne({
            where: {
                email: req.body.email
            }
        }).then(user => {
            if(user) {
                res.status(400).send({
                    message: "Oops! Username already taken!"
                });
                return;
            }
            
            next();
        });
    });
};

// Check If roles exist
checkRolesExisted = (req, res, next) => {
    if(req.body.roles) {
        for(let i = 0; i < req.body.roles.length; i++) {
            if(!ROLES.includes(req.body.roles[i])) {
                res.status(400).send({
                    message: "Oops! Roles doesn't exist = " + req.body.roles[i]
                });
                return;
            }
        }
    }

    next();
};

const verifySignup = {
    checkUsernameOrEmailDupl: checkUsernameOrEmailDupl,
    checkRolesExisted: checkRolesExisted
}

module.exports = verifySignup;