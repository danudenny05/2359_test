const db = require("../models/index");
const { Op } = require("sequelize");
const Task = db.task

checkDuplicateTask = (req, res, next) => {
    // Check Task Exist
    Task.findOne({
        where: {
            [Op.and] : [
                {name: req.body.name},
                {location: req.body.location},
                {date_start: req.body.date_start},
                {time_start: req.body.time_start}
            ]
        }
    }).then(task => {
        if(task) {
            res.status(400).send({
                message: "Oops! Task already exists!"
            });
            return;
        }
    });
    
};

const verifyTask = {
    checkDuplicateTask: checkDuplicateTask
}

module.exports = verifyTask;