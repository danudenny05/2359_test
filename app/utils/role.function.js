const db = require("../models/index");
const Role = db.role;

function roleInit() {
    Role.create({
        id: 1,
        name: "user"
    });

    Role.create({
        id: 2,
        name: "admin"
    })
}

module.exports = roleInit;