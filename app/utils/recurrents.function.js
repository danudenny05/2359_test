const db = require("../models/index");
const Recurrents = db.recurrents;

function recureInit() {
    Recurrents.create({
        id: 1,
        type: "Daily"
    });

    Recurrents.create({
        id: 2,
        name: "Weekly"
    })

    Recurrents.create({
        id: 3,
        type: "Monthly"
    });

    Recurrents.create({
        id: 4,
        name: "Yearly"
    })
}

module.exports = recureInit;