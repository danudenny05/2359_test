module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        username: {
            type:Sequelize.STRING
        },
        email: {
            type:Sequelize.STRING
        },
        password: {
            type:Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at',
        },
        updatedAt: {
            type: Sequelize.DATE,
            field: 'updated_at'
        }
    });

    return User;
}