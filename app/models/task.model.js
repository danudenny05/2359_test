module.exports = (sequelize, Sequelize) => {
    const Task = sequelize.define("tasks", {
        name: {
            type:Sequelize.STRING
        },
        description: {
            type:Sequelize.STRING
        },
        location: {
            type:Sequelize.STRING
        },
        date_start: {
            type:Sequelize.DATEONLY
        },
        date_end: {
            type:Sequelize.DATEONLY
        },
        time_start: {
            type:Sequelize.TIME
        },
        time_end: {
            type:Sequelize.TIME
        },
        is_recurring: {
            type:Sequelize.BOOLEAN
        },
        is_finished: {
            type:Sequelize.BOOLEAN
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at',
        },
        updatedAt: {
            type: Sequelize.DATE,
            field: 'updated_at'
        }
    });

    return Task;
}