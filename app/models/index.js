const Sequelize = require("sequelize");
const dbConf = require("../config/db.config");

const sequelize = new Sequelize(
    dbConf.dbname, dbConf.user, dbConf.pass, {
        host: dbConf.host,
        dialect: dbConf.dialect,
        operatorAliases: false,
        pool: {
            max: dbConf.pool.max,
            min: dbConf.pool.min,
            acquire: dbConf.pool.acquire,
            idle: dbConf.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model")(sequelize, Sequelize);
db.role = require("./role.model")(sequelize, Sequelize);
db.task = require("./task.model")(sequelize, Sequelize);
db.recurrents = require("./recurring.model")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
})

db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
})

db.user.hasMany(db.task, { as: "tasks" });
db.task.belongsTo(db.user, {
  foreignKey: "userId",
  as: "user",
});

db.recurrents.hasMany(db.task, { as: "task" });
db.task.belongsTo(db.recurrents, {
  foreignKey: "recurrentId",
  as: "recurrents",
});

db.ROLES = ["user", "admin"];

module.exports = db;