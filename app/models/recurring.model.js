module.exports = (sequelize, Sequelize) => {
    const Recurrent = sequelize.define("recurrents", {
        type: {
            type:Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at',
        },
        updatedAt: {
            type: Sequelize.DATE,
            field: 'updated_at'
        }
    });

    return Recurrent;
}