const controller = require("../controllers/user.controller");
const authJwt = require("../middleware/authJwt");
var router = require("express").Router();

module.exports = function (app) {
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    // Route List
    router.get("/", [authJwt.verifyToken], controller.getAllUser); //Get All Users
    router.get("/:id", [authJwt.verifyToken], controller.findUserById); // Find Users by ID
    router.put("/:id", [authJwt.verifyToken], controller.updateUser); // Update / Edit a User
    router.delete("/:id", [authJwt.verifyToken], controller.deleteUser); // Delete a User

    app.use('/api/users', router);
};