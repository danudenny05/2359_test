const controller = require("../controllers/task.controller");
const authJwt = require("../middleware/authJwt");
var router = require("express").Router();

module.exports = function (app) {
    // Verifying Jwt TOkens using headers
    app.use(function(req, res, next) {
        res.header(
          "Access-Control-Allow-Headers",
          "token, Origin, Content-Type, Accept" // type "token" in Headers
        );
        next();
    });
    
    // Route List
    router.get("/", [authJwt.verifyToken],  controller.getAllTask); //Get All Task
    router.get("/:id", [authJwt.verifyToken],  controller.findTaskById); // Find Task by ID
    router.post("/", [authJwt.verifyToken], controller.createTask); // Create a Task
    router.post("/quicktask", [authJwt.verifyToken], controller.createQuickTask); // Create a Quick Task with Single String
    router.put("/:id", [authJwt.verifyToken], controller.updateTask); // Update / Edit a Task
    router.delete("/:id", [authJwt.verifyToken], controller.deleteTask); // Delete a Task

    app.use('/api/tasks', router); //base endpoint for All Task Request
};