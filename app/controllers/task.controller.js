const db = require("../models");
const Task = db.task;

const pagination = require("../utils/pagination"); //Pagination Module

const Op = db.Sequelize.Op;

// Get All Task
exports.getAllTask = (req, res) => {
  const {
    page,
    size,
    title
  } = req.query;
  const name = req.query.name;
  const location = req.query.location;
  const time_start = req.query.time_start;
  var condition = name ? {
    name: {
      [Op.like]: `%${name}%`
    }
  } : null || location ? {
    location: {
      [Op.like]: `%${location}%`
    }
  } : null || time_start ? {
    time_start: {
      [Op.like]: `%${time_start}%`
    }
  } : null;
  const {
    limit,
    offset
  } = pagination.getPagination(page, size);

  Task.findAndCountAll({
      where: condition,
      offset: offset,
      limit: limit
    })
    .then(data => {
      const response = pagination.getPagingData(data, page, limit);
      res.send(response);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Find Task by ID
exports.findTaskById = (req, res) => {
  const id = req.params.id;

  Task.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Task with id=" + id
      });
    });
};

// Create a Task
exports.createTask = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Task
  const tasks = {
    name: req.body.name,
    description: req.body.description,
    location: req.body.location,
    date_start: req.body.date_start,
    date_end: req.body.date_end,
    time_start: req.body.time_start,
    time_end: req.body.time_end,
    is_recurring: req.body.is_recurring ? req.body.is_recurring : false,
    is_finished: req.body.is_finished ? req.body.is_finished : false,
    userId: req.body.userId
  };
  // Save Task in the database
  Task.create(tasks)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Task."
      });
    });
};

// Create a Task
exports.createQuickTask = (req, res) => {
  // Validate request
  var fullString = req.body.fullString;
  var splitTaskName = fullString.split('at')[0];
  var splitTaskLocation = fullString.split('at')[1];
  var splitTaskLocation = splitTaskLocation.split('this')[0];
  var splitTaskDay = fullString.split('this')[1];
  var splitTaskDays = splitTaskDay.split('at')[0];
  var splitTaskTime = splitTaskDay.split('at')[1];
  var getAmPm = splitTaskTime.split(/([0-9]+)/)[2];

  
  if (getAmPm == 'am') {
    var splitTaskTimes = splitTaskTime.split('am')[0];
    if(splitTaskTimes < 10) {
      var timeStringTrim = 0 + splitTaskTimes.trim() + ':00:00'
    } else {
      var timeStringTrim = splitTaskTimes.trim() + ':00:00'
    }
  } else {
    var splitTaskTimes = splitTaskTime.split('pm')[0];
    if(splitTaskTimes < 10) {
      var timeStringTrim = 0 + splitTaskTimes.trim() + ':00:00'
    } else {
      var timeStringTrim = splitTaskTimes.trim() + ':00:00'
    }
  }
  const tasks = {
    name: splitTaskName,
    location: splitTaskLocation,
    time_start: timeStringTrim,
    userId: req.body.userId
  };

  // Save Task in the database
  Task.create(tasks)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Task."
      });
    });

  
};

// Update a Task
exports.updateTask = (req, res) => {
  const id = req.params.id;

  Task.update(req.body, {
      where: {
        id: id
      }
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Task was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Task with id=${id}. Maybe Task was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Task with id=" + id
      });
    });
};

// Delete a Task
exports.deleteTask = (req, res) => {
  const id = req.params.id;

  Task.destroy({
      where: {
        id: id
      }
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Task was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Task with id=${id}. Maybe Task was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Task with id=" + id
      });
    });
};