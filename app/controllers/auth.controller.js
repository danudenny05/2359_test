const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

const jwt = require("jsonwebtoken");
const argon2 = require('argon2');

exports.signup = async (req, res) => {
    const hashedPassword = await argon2.hash(req.body.password) //Hashing password using Argon2

    // Save User to Database
    User.create({
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword
        })
        .then(user => {
            if (req.body.roles) {
                Role.findAll({
                    where: {
                        name: {
                            [Op.or]: req.body.roles
                        }
                    }
                }).then(roles => {
                    user.setRoles(roles).then(() => {
                        res.send({
                            message: "User was registered successfully!"
                        });
                    });
                });
            } else {
                // user role = 1
                user.setRoles([1]).then(() => {
                    res.send({
                        message: "User was registered successfully!"
                    });
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        });
};

exports.signin = (req, res) => {
    User.findOne({
            where: {
                username: req.body.username
            }
        })
        .then(async user => {
            if (!user) {
                return res.status(404).send({
                    message: "User Not found."
                });
            }

            var passwordIsValid = await argon2.verify(
                user.password,
                req.body.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            var token = jwt.sign({
                id: user.id,
                username: user.username,
                loginTime: new Date().toLocaleString()
            }, config.secretKey, {
                expiresIn: 86400 // 24 hours
            });

            var authorities = [];
            user.getRoles().then(roles => {
                for (let i = 0; i < roles.length; i++) {
                    authorities.push("ROLE_" + roles[i].name.toUpperCase());
                }
                res.status(200).send({
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    roles: authorities,
                    accessToken: token,
                    loginTime: new Date().toLocaleString()
                });
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message
            });
        });
};