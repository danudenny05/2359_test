const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Task = db.task;

const pagination = require("../utils/pagination");

const Op = db.Sequelize.Op;

// Get All Users
exports.getAllUser = (req, res) => {
    const {
        page,
        size,
        title
    } = req.query;
    const username = req.query.username;
    var condition = username ? {
        username: {
            [Op.like]: `%${username}%`
        }
    } : null;
    const {
        limit,
        offset
    } = pagination.getPagination(page, size);


    User.findAndCountAll({
            where: condition,
            include: [{
                model: Task,
                as: 'tasks'
            }],
            offset: offset,
            limit: limit
        })
        .then(data => {
            const response = pagination.getPagingData(data, page, limit);
            res.send(response);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
}

// Find Task by ID
exports.findUserById = (req, res) => {
    const id = req.params.id;
  
    User.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving User with id=" + id
        });
      });
};

// Update a User
exports.updateUser = (req, res) => {
    const id = req.params.id;
  
    User.update(req.body, {
        where: {
          id: id
        }
      })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
};
  
// Delete a User
exports.deleteUser = (req, res) => {
    const id = req.params.id;
  
    User.destroy({
        where: {
          id: id
        }
      })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete User with id=${id}. Maybe User was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete User with id=" + id
        });
      });
};